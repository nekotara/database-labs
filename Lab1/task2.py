from scrapy import cmdline
import os
import lxml.etree as ET


def crawl():
    try:
        os.remove("results/delivery.xml")
    except OSError:
        print("results/delivery.xml not found")
    cmdline.execute("scrapy crawl delivery -o results/delivery.xml -t xml".split())


def xslt_parse():
    dom = ET.parse('results/delivery.xml')
    xslt = ET.parse('delivery.xslt')
    transform = ET.XSLT(xslt)
    newdom = transform(dom)
    with open('results/delivery.html', 'wb') as f:
        f.write(ET.tostring(newdom, pretty_print=True))

# crawl()
xslt_parse()
