from lxml import etree
from scrapy import cmdline

# cmdline.execute("scrapy crawl doroga".split())
root = None
with open('results/doroga.xml', 'r', encoding="utf8") as file:
    pages = etree.parse(file).xpath('page')

def counter(el):
    return int(el.xpath("count(fragment[@type='image'])"))

page = min(pages, key=counter)
rez = page.xpath('./@url')
number = counter(page)

print('Page with the least of grahic elements: ')
print(rez, number, page.xpath("./@url"))
