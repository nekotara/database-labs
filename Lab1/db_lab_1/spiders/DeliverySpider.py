import scrapy


class DeliverySpider(scrapy.Spider):
    name = "delivery"
    fields = {
        'link_pagination': '//ul[@class="pagination"]//a/@href',
        'link_category': '//ul[@class="breadcrumb"]//li[@class="active"]/a/@href',
        'product': '//div[@class="product-thumb thumbnail "]',
        'price': './/p[@class="price"]/text()',
        'name': './/div[@class="caption"]//h4/a/text()',
        'img': './/img[@class="img-responsive center-block"]/@src',
        'product_link': './/div[@class="caption"]//h4/a/@href'
    }
    custom_settings = {
        'CLOSESPIDER_PAGECOUNT': 0,
        'CLOSESPIDER_ITEMCOUNT': 20
    }
    start_urls = [
        'http://freedelivery.com.ua/arduino-100/'
    ]
    allowed_domains = [
        'freedelivery.com.ua'
    ]

    def parse(self, response):
        for product in response.xpath(self.fields["product"]):
            yield {
                'link': product.xpath(self.fields['product_link']).extract(),
                'price': product.xpath(self.fields['price']).extract(),
                'img': product.xpath(self.fields['img']).extract(),
                'name': ''.join(product.xpath(self.fields['name']).extract())
            }
        for a in response.xpath(self.fields["link_category"]):
            yield response.follow(a.extract(), callback=self.parse)
        for a in response.xpath(self.fields["link_pagination"]):
            yield response.follow(a.extract(), callback=self.parse)
